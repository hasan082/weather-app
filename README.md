# Weather App

This is a Flutter app that displays weather information using data from the OpenWeatherMap API. It utilizes the `http`, `geolocator`, and `permission_handler` packages.

## Features

- Current weather conditions
- Temperature in Celsius
- Weather icons for different conditions
- Location-based weather data

## Installation

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/hasan082/weather-app.git
   ```

2. Install the dependencies:

   ```shell
   flutter pub get
   ```

3. Run the app:

   ```shell
   flutter run
   ```

## Configuration

To use the OpenWeatherMap API, you need to obtain an API key. Follow these steps:

1. Sign up for an account at [OpenWeatherMap](https://openweathermap.org/).
2. Obtain an API key from the dashboard.
3. Go to `repo` folder then  `weather_repo`.
4. Change your api key like below:

   ```
   apiKey=your-api-key
   ```

5. Replace `your-api-key` with your actual API key.

## Permissions

The app requires the following permissions:

- Location permission: Used to fetch weather data based on the user's current location.

## Packages Used

- `http`: Used for making HTTP requests to the OpenWeatherMap API.
- `geolocator`: Used for obtaining the user's current location.
- `permission_handler`: Used for handling location permission requests.

## Screenshots
<img src="https://gitlab.com/hasan082/weather-app/-/raw/master/splashscreen.png" width="280" />
<img src="https://gitlab.com/hasan082/weather-app/-/raw/master/weather.jpeg" width="280" />


## Contributing

Contributions are welcome! If you have any bug fixes, enhancements, or new features, please open an issue or submit a pull request.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit/).
```
