import 'package:flutter/material.dart';


Widget getWeatherIcon(String condition, {double size = 24}) {
  String imagePath = 'assets/weathericon/';

  switch (condition) {
    case 'Thunderstorm':
      imagePath += 'thunder.png';
      break;
    case 'Drizzle':
      imagePath += 'raining.png';
      break;
    case 'Rain':
      imagePath += 'raining.png';
      break;
    case 'Snow':
      imagePath += 'snow.png';
      break;
    case 'Mist':
    case 'Smoke':
    case 'Haze':
    case 'Dust':
    case 'Fog':
    case 'Sand':
    case 'Ash':
      imagePath += 'cloudyday.png';
      break;
    case 'Squall':
    case 'Tornado':
      imagePath += 'thunder.png';
      break;
    case 'Clear':
      imagePath += 'sun.png';
      break;
    case 'Clouds':
      imagePath += 'cloud.png';
      break;
    default:
      imagePath += 'question.png';
  }

  return Image.asset(
    imagePath,
    height: size,
    width: size,
  );
}
