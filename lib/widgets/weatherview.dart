import 'package:flutter/material.dart';

import '../models/weather_models.dart';
import '../utils/wether_icon_utils.dart';

class WeatherDataView extends StatelessWidget {
  final MyWeather weather;

  const WeatherDataView({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    String getFormattedUpdateTime(int unixTimestamp) {
      final updateTime = DateTime.fromMillisecondsSinceEpoch(
          unixTimestamp * 1000,
          isUtc: true);
      final hour = updateTime.toLocal().hour;
      final minute = updateTime.toLocal().minute.toString().padLeft(2, '0');
      final period = hour < 12 ? 'AM' : 'PM';
      final formattedHour = hour > 12 ? (hour - 12) : hour;
      final formattedTime = '$formattedHour:$minute $period';
      return formattedTime;
    }

    String getDayName(int unixTimestamp) {
      final dateTime = DateTime.fromMillisecondsSinceEpoch(unixTimestamp * 1000, isUtc: true);
      final weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      final dayIndex = dateTime.toLocal().weekday;
      final dayName = weekdays[dayIndex - 1];
      return dayName;
    }



    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: const AssetImage('assets/images/weatherbg.jpg'),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(
            Colors.black.withOpacity(.48),
            BlendMode.overlay,
          ),
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 25,
            ),
            Text(
              '${weather.name ?? 0}',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              getDayName(weather.dt ?? 0),
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 30,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              'Updated: ${getFormattedUpdateTime(weather.dt ?? 0)}',
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              child: getWeatherIcon(weather.weather?[0].main ?? '', size: 100),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              "${weather.main!.temp?.toStringAsFixed(0) ?? '0'}\u00B0C",
              style: const TextStyle(fontSize: 70, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 15,
            ),
            Text(
              weather.weather![0].main ?? '0',
              style: const TextStyle(fontSize: 50, fontWeight: FontWeight.normal),
            ),
            const SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Image.asset("assets/weathericon/tempmax.png", width: 15),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Max: ${weather.main!.tempMin}"),
                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/weathericon/tempmin.png", width: 15),
                    const SizedBox(
                      width: 10,
                    ),
                    Text("Min: ${weather.main!.tempMin}"),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
