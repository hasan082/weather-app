import 'package:flutter/material.dart';

class WeatherLoadingScreen extends StatelessWidget {
  const WeatherLoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: const BoxDecoration(
        color: Color(0xFF2B3155),
      ),
      child: const Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(color: Colors.white,),
            SizedBox(height: 15),
            Text(
              'Loading....',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
