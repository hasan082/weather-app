import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weatherapp/controller/weather_controller.dart';
import 'package:weatherapp/models/weather_models.dart';
import '../widgets/weather_loading-widget.dart';
import '../widgets/weatherview.dart';

class WeatherScreen extends StatefulWidget {
  const WeatherScreen({Key? key}) : super(key: key);

  @override
  State<WeatherScreen> createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen>
    with SingleTickerProviderStateMixin {
  bool? isLoading;
  late MyWeather weather;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    getLatLon();
  }

  //Retrieve the data using weather controller============
  getCurrentLocation(double lat, double lon) async {
    MyWeather myWeather =
        await WeatherController().getMyCurrentWeatherCtl(lat, lon);
    if (myWeather.cod == 200 && mounted) {
      setState(() {
        weather = myWeather;
        isLoading = false;
      });
    }
  }

  //Getting the user location using geolocation Package===========
  getLatLon() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      getCurrentLocation(position.latitude, position.longitude);
      print(position);
    } catch (error) {
      print(error.toString());
    }
  }

  //Formatting the date as Required========



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF2B3155),
        title: const Text('Weather App'),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.settings),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      body: SafeArea(
        child: isLoading == false
            ? WeatherDataView(weather: weather,)
            : const WeatherLoadingScreen()
      ),
    );
  }
}
