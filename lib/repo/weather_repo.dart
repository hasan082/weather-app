import 'package:http/http.dart';

class WeatherRepository{

  final apiKey = '2be8b1f37693e21f54d2e071d3429e46';

  Future<Response> getMyCurrentWeatherRepo(double lat, double lon) async{
    Response response = await Client().get(
      Uri.parse(
          'https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&units=metric&appid=$apiKey'),
    );
    return response;
  }
}
